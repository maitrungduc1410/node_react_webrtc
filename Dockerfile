FROM node:lts-alpine

LABEL maintainer="Mai Trung Duc (maitrungduc1410@gmail.com)"

WORKDIR /app

COPY . .

RUN apk add --no-cache git

RUN npm install --no-package-lock && \
    npm run build && \
    npm install -g pm2

CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]
var express = require('express')
var router = express.Router()

const chunksList = require('../public/js/chunks-list.json').filter(chunk => {
  if (chunk.startsWith('main') || chunk.startsWith('runtime')) {
    return chunk
  }
})
/* GET home page. */
router.get('/*', function (req, res, next) {
  res.render('index', { title: 'Express', chunksList: chunksList })
})

module.exports = router

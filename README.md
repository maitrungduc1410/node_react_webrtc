# Introduction
A project demonstrate WebRTC
# Backend
Nodejs, express
# Frontend
ReactJS
# Setup
Run `npm install` to install dependencies
# Run
- For development: `npm run dev`
- For production: `npm run prod`
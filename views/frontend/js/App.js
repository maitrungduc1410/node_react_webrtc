import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// import Home from './components/home/Home'
// import Room from './components/room/Room'
// import RouteNotFound from './components/404/RouteNotFound.js'

const Home = React.lazy(() => import('./components/home/Home'))
const Room = React.lazy(() => import('./components/room/Room'))
const RouteNotFound = React.lazy(() => import('./components/404/RouteNotFound.js'))

function App (props) {
  return (
    <div>
      <Switch>
        <Route
          exact
          path="/"
          render={props => (
            <Suspense fallback={<div>Loading...</div>}>
              <Home {...props} />
            </Suspense>
          )
          }
        />
        <Route
          exact
          path="/room/:id"
          render={props => (
            <Suspense fallback={<div>Loading...</div>}>
              <Room {...props} />
            </Suspense>
          )
          }
        />
        <Route render={props => (
          <Suspense fallback={<div>Loading...</div>}>
            <RouteNotFound {...props} />
          </Suspense>
        )} />
      </Switch>
    </div>
  )
}

export default App

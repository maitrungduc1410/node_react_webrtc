import React, { Component } from 'react'
import { Grid, Fab, Snackbar, IconButton, Link } from '@material-ui/core'
import styles from './Room.module.scss'
import Peer from 'peerjs'
import CloseIcon from '@material-ui/icons/Close'
import PropTypes from 'prop-types'

class Room extends Component {
  constructor (props) {
    super(props)

    this.state = {
      peer: {},
      settings: {
        video: true,
        audio: true,
        fullScreen: false
      },
      isSnackBarOpen: false,
      message: '',
      isConnectedWithOtherPeer: false,
      baseUrl: `${window.location.protocol}//${window.location.hostname}:${window.location.port}`,
      mediaStream: null
    }
  }

  componentDidMount () {
    this.config()

    const videoMiniEl = document.getElementById(styles['video-mini'])
    navigator.mediaDevices.getUserMedia({ video: this.state.settings.video, audio: this.state.settings.audio })
      .then(stream => {
        videoMiniEl.srcObject = stream
        this.setState({
          mediaStream: stream
        })

        let peer

        if (this.props.location.state && this.props.location.state.roomID) {
          peer = this.initPeer(this.props.match.params.id, videoMiniEl)
        } else {
          peer = new Peer()
          peer.on('open', (id) => {
            const videoEl = document.getElementById(styles['video-container'])
            var call = peer.call(this.props.match.params.id, videoMiniEl.srcObject)
            call.on('stream', (remoteStream) => {
              videoEl.srcObject = remoteStream
            })
          })

          this.setState({
            peer: peer,
            isConnectedWithOtherPeer: true
          })
        }

        peer.on('error', (err) => {
          if (err.type === 'peer-unavailable') {
            peer = this.initPeer(this.props.match.params.id, videoMiniEl)
          } else if (err.type === 'unavailable-id') {
            this.setState({
              isSnackBarOpen: true,
              message: 'Your peer ID is already exists!'
            })
            console.log(err.type)
            // location.replace(location.href.split("#")[0])
          }
        })
      }).catch((err) => {
        console.log('Failed to get local stream', err)
      })
  }

  initPeer (peerId, videoMiniEl) {
    const peer = new Peer(peerId)
    peer.on('open', (id) => {
      peer.on('call', (call) => {
        const videoEl = document.getElementById(styles['video-container'])
        call.answer(videoMiniEl.srcObject)
        call.on('stream', (remoteStream) => {
          videoEl.srcObject = remoteStream
          this.setState({
            isConnectedWithOtherPeer: true
          })
        })
      })
    })

    this.setState({
      peer: peer
    })

    return peer
  }

  toggleVideo = (type) => {
    const settings = { ...this.state.settings }

    settings[type] = !settings[type]

    this.setState({
      settings: { ...settings }
    })
  }

  toggleFullScreen = () => {
    let isFullScreen
    if (document.fullscreenElement) {
      document.exitFullscreen()
      isFullScreen = false
    } else {
      document.documentElement.requestFullscreen()
      isFullScreen = true
    }

    const settings = { ...this.state.settings }

    settings.fullScreen = isFullScreen

    this.setState({
      settings: { ...settings }
    })
  }

  config () {
    if (!document.fullscreenElement) {
      Object.defineProperty(document, 'fullscreenElement', {
        get: function () {
          return document.mozFullScreenElement || document.msFullscreenElement || document.webkitFullscreenElement
        }
      })

      Object.defineProperty(document, 'fullscreenEnabled', {
        get: function () {
          return document.mozFullScreenEnabled || document.msFullscreenEnabled || document.webkitFullscreenEnabled
        }
      })
    }

    if (!Element.prototype.requestFullscreen) {
      Element.prototype.requestFullscreen = Element.prototype.mozRequestFullscreen || Element.prototype.webkitRequestFullscreen || Element.prototype.msRequestFullscreen
    }

    if (!document.exitFullscreen) {
      document.exitFullscreen = document.mozExitFullscreen || document.webkitExitFullscreen || document.msExitFullscreen
    }
  }

  closeConnection = () => {
    this.state.peer.destroy()
  }

  handleCloseSnackBar = () => {
    this.setState({
      isSnackBarOpen: false
    })
  }

  componentWillUnmount () {
    this.state.mediaStream.getTracks().forEach(track => { // stop video and audio recording
      track.stop()
    })

    this.closeConnection()
  }

  render () {
    let videoEl, audioEl
    if (this.state.settings.video) {
      videoEl = <i className="fas fa-video"></i>
    } else {
      videoEl = <i className="fas fa-video-slash"></i>
    }

    if (this.state.settings.audio) {
      audioEl = <i className="fas fa-microphone-alt"></i>
    } else {
      audioEl = <i className="fas fa-microphone-alt-slash"></i>
    }

    return (
      <Grid container className={styles['room-container']} spacing={2} alignContent="center" justify="center">
        <div className={styles['video-wrapper']}>
          <video autoPlay playsInline id={styles['video-container']}></video>
          <video autoPlay playsInline muted id={styles['video-mini']}></video>
        </div>
        <div className={styles['action-buttons']}>
          <Fab onClick={this.toggleVideo.bind(this, 'audio')} color="primary" aria-label="SettingsPhone" className={styles['button-item']}>
            { audioEl }
          </Fab>
          <Fab onClick={this.toggleVideo.bind(this, 'video')} color="primary" aria-label="SettingsPhone" className={styles['button-item']}>
            { videoEl }
          </Fab>
          <Fab onClick={this.toggleFullScreen} color="primary" aria-label="SettingsPhone" className={styles['button-item']}>
            <i className="fas fa-expand"></i>
          </Fab>
          <Fab onClick={this.closeConnection} color="secondary" aria-label="SettingsPhone" className={styles['button-item']}>
            <i className="fas fa-phone-slash"></i>
          </Fab>
        </div>
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={this.state.isSnackBarOpen}
          autoHideDuration={10000}
          onClose={this.handleCloseSnackBar}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">{this.state.message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.handleCloseSnackBar}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          autoHideDuration={null}
          open={!this.state.isConnectedWithOtherPeer}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={
            <span id="message-id">
              Waiting for other peer to connect in
              <a href={this.state.baseUrl + '/room/' + this.state.peer.id}></a>
            </span>
          }
          action={[
            <Link rel="noopener" target="_blank" key="undo" href={this.state.baseUrl + '/room/' + this.state.peer.id} color="secondary" size="small">
              {`${this.state.baseUrl}/room/${this.state.peer.id}`}
            </Link>
          ]}
        />
      </Grid>
    )
  }
}

Room.propTypes = {
  location: PropTypes.any,
  match: PropTypes.any
}

export default Room

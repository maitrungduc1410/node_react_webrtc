import React, { Component } from 'react'
import { Grid, Button, TextField } from '@material-ui/core'
import styles from './Home.module.scss'
import PropTypes from 'prop-types'

class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      roomID: ''
    }
  }

  createRoom = () => {
    this.props.history.push(`/room/${this.state.roomID}`, { roomID: this.state.roomID })
  }

  onTextFieldChange = (e) => {
    this.setState({
      roomID: e.target.value
    })
  }

  render () {
    return (
      <Grid container className={styles.app} spacing={2} alignContent="center" justify="center">
        <Grid item xs={12} container alignContent="center" justify="center">
          <h2 className={styles['app-title']}>Web RTC Demo</h2>
        </Grid>
        <Grid item xs={12} container alignContent="center" justify="center">
          <TextField
            label="Peer ID"
            variant="outlined"
            onChange={this.onTextFieldChange}
          />
          <Button variant="contained" color="primary" onClick={this.createRoom}>
            Create
          </Button>
        </Grid>
      </Grid>
    )
  }
}

Home.propTypes = {
  history: PropTypes.any
}

export default Home
